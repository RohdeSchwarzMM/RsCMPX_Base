RxImage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:TRACe:RXIMage

.. code-block:: python

	FETCh:BASE:SALignment:TRACe:RXIMage



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Trace.RxImage.RxImageCls
	:members:
	:undoc-members:
	:noindex: