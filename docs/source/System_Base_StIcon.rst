StIcon
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:BASE:STICon:ENABle
	single: SYSTem:BASE:STICon:OPEN
	single: SYSTem:BASE:STICon:CLOSe

.. code-block:: python

	SYSTem:BASE:STICon:ENABle
	SYSTem:BASE:STICon:OPEN
	SYSTem:BASE:STICon:CLOSe



.. autoclass:: RsCMPX_Base.Implementations.System.Base.StIcon.StIconCls
	:members:
	:undoc-members:
	:noindex: