Process
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:COMPass:STATistics:PROCess

.. code-block:: python

	DIAGnostic:COMPass:STATistics:PROCess



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Compass.Statistics.Process.ProcessCls
	:members:
	:undoc-members:
	:noindex: