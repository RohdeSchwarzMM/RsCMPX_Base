Base
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Sense.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Base_IpSet.rst
	Sense_Base_Reference.rst
	Sense_Base_Temperature.rst