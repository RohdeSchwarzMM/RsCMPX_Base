Selftest
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INIT:SELFtest

.. code-block:: python

	INIT:SELFtest



.. autoclass:: RsCMPX_Base.Implementations.Init.Selftest.SelftestCls
	:members:
	:undoc-members:
	:noindex: