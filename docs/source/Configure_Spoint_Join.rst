Join
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SPOint:JOIN

.. code-block:: python

	CONFigure:SPOint:JOIN



.. autoclass:: RsCMPX_Base.Implementations.Configure.Spoint.Join.JoinCls
	:members:
	:undoc-members:
	:noindex: