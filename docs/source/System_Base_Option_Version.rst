Version
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:OPTion:VERSion

.. code-block:: python

	SYSTem:BASE:OPTion:VERSion



.. autoclass:: RsCMPX_Base.Implementations.System.Base.Option.Version.VersionCls
	:members:
	:undoc-members:
	:noindex: