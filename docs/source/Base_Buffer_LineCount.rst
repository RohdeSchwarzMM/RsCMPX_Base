LineCount
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:BUFFer:LINecount

.. code-block:: python

	FETCh:BASE:BUFFer:LINecount



.. autoclass:: RsCMPX_Base.Implementations.Base.Buffer.LineCount.LineCountCls
	:members:
	:undoc-members:
	:noindex: