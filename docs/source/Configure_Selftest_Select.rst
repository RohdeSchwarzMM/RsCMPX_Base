Select
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SELFtest:SELect

.. code-block:: python

	CONFigure:SELFtest:SELect



.. autoclass:: RsCMPX_Base.Implementations.Configure.Selftest.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: