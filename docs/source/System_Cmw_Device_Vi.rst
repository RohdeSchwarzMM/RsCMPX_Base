Vi
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:CMW:DEVice:VI:MODE
	single: SYSTem:CMW:DEVice:VI:COUNt

.. code-block:: python

	SYSTem:CMW:DEVice:VI:MODE
	SYSTem:CMW:DEVice:VI:COUNt



.. autoclass:: RsCMPX_Base.Implementations.System.Cmw.Device.Vi.ViCls
	:members:
	:undoc-members:
	:noindex: