RsCMPX_Base Utilities
==========================

.. _Utilities:

.. autoclass:: RsCMPX_Base.CustomFiles.utilities.Utilities()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
   :member-order: bysource
