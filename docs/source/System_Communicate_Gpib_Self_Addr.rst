Addr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:GPIB<inst>[:SELF]:ADDR

.. code-block:: python

	SYSTem:COMMunicate:GPIB<inst>[:SELF]:ADDR



.. autoclass:: RsCMPX_Base.Implementations.System.Communicate.Gpib.Self.Addr.AddrCls
	:members:
	:undoc-members:
	:noindex: