TxFilter<TxFilter>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr10
	rc = driver.base.correction.ifEqualizer.trace.gdelay.corrected.slot.txFilter.repcap_txFilter_get()
	driver.base.correction.ifEqualizer.trace.gdelay.corrected.slot.txFilter.repcap_txFilter_set(repcap.TxFilter.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:CORRection:IFEQualizer:TRACe:GDELay:CORRected:SLOT<Slot>:TXFilter<Filter>

.. code-block:: python

	FETCh:BASE:CORRection:IFEQualizer:TRACe:GDELay:CORRected:SLOT<Slot>:TXFilter<Filter>



.. autoclass:: RsCMPX_Base.Implementations.Base.Correction.IfEqualizer.Trace.Gdelay.Corrected.Slot.TxFilter.TxFilterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.correction.ifEqualizer.trace.gdelay.corrected.slot.txFilter.clone()