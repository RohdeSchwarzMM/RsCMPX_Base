Tzone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:TZONe

.. code-block:: python

	SYSTem:TZONe



.. autoclass:: RsCMPX_Base.Implementations.System.Tzone.TzoneCls
	:members:
	:undoc-members:
	:noindex: