Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BASE:EXTA:CATalog:SOURce

.. code-block:: python

	TRIGger:BASE:EXTA:CATalog:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.Base.ExtA.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: