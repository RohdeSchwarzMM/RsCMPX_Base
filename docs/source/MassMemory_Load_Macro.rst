Macro
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:LOAD:MACRo

.. code-block:: python

	MMEMory:LOAD:MACRo



.. autoclass:: RsCMPX_Base.Implementations.MassMemory.Load.Macro.MacroCls
	:members:
	:undoc-members:
	:noindex: