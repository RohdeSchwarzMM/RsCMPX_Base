Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:LENGth

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:LENGth



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.CorrectionTable.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: