Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BASE:UINitiated<n>:EXECute

.. code-block:: python

	TRIGger:BASE:UINitiated<n>:EXECute



.. autoclass:: RsCMPX_Base.Implementations.Trigger.Base.Uinitiated.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: