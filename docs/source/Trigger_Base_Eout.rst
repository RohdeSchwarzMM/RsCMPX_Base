Eout<Eout>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.trigger.base.eout.repcap_eout_get()
	driver.trigger.base.eout.repcap_eout_set(repcap.Eout.Nr1)





.. autoclass:: RsCMPX_Base.Implementations.Trigger.Base.Eout.EoutCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.base.eout.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Base_Eout_Catalog.rst
	Trigger_Base_Eout_Source.rst