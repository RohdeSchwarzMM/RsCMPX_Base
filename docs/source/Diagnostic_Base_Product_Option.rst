Option
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Base.Product.Option.OptionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.base.product.option.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Base_Product_Option_Factory.rst