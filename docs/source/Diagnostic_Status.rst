Status
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:STATus:OPC

.. code-block:: python

	DIAGnostic:STATus:OPC



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Status.StatusCls
	:members:
	:undoc-members:
	:noindex: