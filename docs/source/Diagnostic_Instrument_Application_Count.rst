Count
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:INSTrument:APPLication:COUNt

.. code-block:: python

	DIAGnostic:INSTrument:APPLication:COUNt



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Instrument.Application.Count.CountCls
	:members:
	:undoc-members:
	:noindex: