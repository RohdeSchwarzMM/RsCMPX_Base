Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:POWer:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.Gprf.Measurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.measurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Measurement_Power_Catalog.rst