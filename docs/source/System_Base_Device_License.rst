License
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:DEVice:LICense

.. code-block:: python

	SYSTem:BASE:DEVice:LICense



.. autoclass:: RsCMPX_Base.Implementations.System.Base.Device.License.LicenseCls
	:members:
	:undoc-members:
	:noindex: