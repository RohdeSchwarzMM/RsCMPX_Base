Magnitude
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Base.Correction.IfEqualizer.Trace.Magnitude.MagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.correction.ifEqualizer.trace.magnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Base_Correction_IfEqualizer_Trace_Magnitude_Corrected.rst
	Base_Correction_IfEqualizer_Trace_Magnitude_Uncorrected.rst