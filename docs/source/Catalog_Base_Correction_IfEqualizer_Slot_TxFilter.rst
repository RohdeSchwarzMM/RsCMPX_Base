TxFilter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:BASE:CORRection:IFEQualizer:SLOT<Slot>:TXFilter

.. code-block:: python

	CATalog:BASE:CORRection:IFEQualizer:SLOT<Slot>:TXFilter



.. autoclass:: RsCMPX_Base.Implementations.Catalog.Base.Correction.IfEqualizer.Slot.TxFilter.TxFilterCls
	:members:
	:undoc-members:
	:noindex: