Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BASE:EOUT<n>:SOURce

.. code-block:: python

	TRIGger:BASE:EOUT<n>:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.Base.Eout.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: