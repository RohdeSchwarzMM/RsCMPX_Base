RxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:TRACe:RXDC

.. code-block:: python

	FETCh:BASE:SALignment:TRACe:RXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Trace.RxDc.RxDcCls
	:members:
	:undoc-members:
	:noindex: