Display
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:BASE:DISPlay:MWINdow
	single: SYSTem:BASE:DISPlay:COLorset
	single: SYSTem:BASE:DISPlay:FONTset
	single: SYSTem:BASE:DISPlay:ROLLkeymode
	single: SYSTem:BASE:DISPlay:LANGuage

.. code-block:: python

	SYSTem:BASE:DISPlay:MWINdow
	SYSTem:BASE:DISPlay:COLorset
	SYSTem:BASE:DISPlay:FONTset
	SYSTem:BASE:DISPlay:ROLLkeymode
	SYSTem:BASE:DISPlay:LANGuage



.. autoclass:: RsCMPX_Base.Implementations.System.Base.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex: