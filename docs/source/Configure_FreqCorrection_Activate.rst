Activate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:FDCorrection:ACTivate

.. code-block:: python

	CONFigure:FDCorrection:ACTivate



.. autoclass:: RsCMPX_Base.Implementations.Configure.FreqCorrection.Activate.ActivateCls
	:members:
	:undoc-members:
	:noindex: