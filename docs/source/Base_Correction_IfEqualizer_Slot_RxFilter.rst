RxFilter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter

.. code-block:: python

	FETCh:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter



.. autoclass:: RsCMPX_Base.Implementations.Base.Correction.IfEqualizer.Slot.RxFilter.RxFilterCls
	:members:
	:undoc-members:
	:noindex: