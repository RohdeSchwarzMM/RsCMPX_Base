RxFilter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter

.. code-block:: python

	CATalog:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter



.. autoclass:: RsCMPX_Base.Implementations.Catalog.Base.Correction.IfEqualizer.Slot.RxFilter.RxFilterCls
	:members:
	:undoc-members:
	:noindex: