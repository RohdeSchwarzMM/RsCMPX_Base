TxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:LLIMit:TXDC

.. code-block:: python

	FETCh:BASE:SALignment:LLIMit:TXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Llimit.TxDc.TxDcCls
	:members:
	:undoc-members:
	:noindex: