Salignment
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:BASE:SALignment
	single: ABORt:BASE:SALignment
	single: STOP:BASE:SALignment
	single: FETCh:BASE:SALignment

.. code-block:: python

	INITiate:BASE:SALignment
	ABORt:BASE:SALignment
	STOP:BASE:SALignment
	FETCh:BASE:SALignment



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.SalignmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.salignment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Base_Salignment_Llimit.rst
	Base_Salignment_Lvalid.rst
	Base_Salignment_Reliability.rst
	Base_Salignment_State.rst
	Base_Salignment_Trace.rst
	Base_Salignment_Ulimit.rst
	Base_Salignment_Xvalues.rst