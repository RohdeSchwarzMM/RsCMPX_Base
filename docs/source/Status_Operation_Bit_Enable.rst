Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:OPERation:BIT<bitno>:ENABle

.. code-block:: python

	STATus:OPERation:BIT<bitno>:ENABle



.. autoclass:: RsCMPX_Base.Implementations.Status.Operation.Bit.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: