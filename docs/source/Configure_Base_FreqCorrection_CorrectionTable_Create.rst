Create
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:CREate

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:CREate



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.CorrectionTable.Create.CreateCls
	:members:
	:undoc-members:
	:noindex: