Description
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SELFtest:INFO:DESCription

.. code-block:: python

	CONFigure:SELFtest:INFO:DESCription



.. autoclass:: RsCMPX_Base.Implementations.Configure.Selftest.Info.Description.DescriptionCls
	:members:
	:undoc-members:
	:noindex: