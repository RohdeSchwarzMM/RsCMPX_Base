Device
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:CMWS:DEVice:ID

.. code-block:: python

	SYSTem:CMWS:DEVice:ID



.. autoclass:: RsCMPX_Base.Implementations.System.SingleCmw.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: