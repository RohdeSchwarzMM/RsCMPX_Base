Procedure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:CMWD

.. code-block:: python

	PROCedure:CMWD



.. autoclass:: RsCMPX_Base.Implementations.Procedure.ProcedureCls
	:members:
	:undoc-members:
	:noindex: