Port
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:SOCKet<inst>:PORT

.. code-block:: python

	SYSTem:COMMunicate:SOCKet<inst>:PORT



.. autoclass:: RsCMPX_Base.Implementations.System.Communicate.Socket.Port.PortCls
	:members:
	:undoc-members:
	:noindex: