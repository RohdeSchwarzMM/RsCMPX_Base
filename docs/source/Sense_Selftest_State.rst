State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SELFtest:STATe:SUM

.. code-block:: python

	SENSe:SELFtest:STATe:SUM



.. autoclass:: RsCMPX_Base.Implementations.Sense.Selftest.State.StateCls
	:members:
	:undoc-members:
	:noindex: