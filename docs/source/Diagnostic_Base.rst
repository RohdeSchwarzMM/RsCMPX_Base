Base
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Base_Mmi.rst
	Diagnostic_Base_Product.rst
	Diagnostic_Base_Salignment.rst