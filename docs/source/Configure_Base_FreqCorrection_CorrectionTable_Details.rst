Details
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:DETails

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:DETails



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.CorrectionTable.Details.DetailsCls
	:members:
	:undoc-members:
	:noindex: