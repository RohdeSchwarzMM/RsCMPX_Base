Display
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DISPlay:UPDate

.. code-block:: python

	SYSTem:DISPlay:UPDate



.. autoclass:: RsCMPX_Base.Implementations.System.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.display.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Display_Monitor.rst