RxImage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:XVALues:RXIMage

.. code-block:: python

	FETCh:BASE:SALignment:XVALues:RXIMage



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Xvalues.RxImage.RxImageCls
	:members:
	:undoc-members:
	:noindex: