FirmwareUpdate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:FWUPdate:INFO

.. code-block:: python

	SENSe:FWUPdate:INFO



.. autoclass:: RsCMPX_Base.Implementations.Sense.FirmwareUpdate.FirmwareUpdateCls
	:members:
	:undoc-members:
	:noindex: