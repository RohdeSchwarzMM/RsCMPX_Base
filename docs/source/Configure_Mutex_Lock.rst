Lock
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:MUTex:LOCK

.. code-block:: python

	CONFigure:MUTex:LOCK



.. autoclass:: RsCMPX_Base.Implementations.Configure.Mutex.Lock.LockCls
	:members:
	:undoc-members:
	:noindex: