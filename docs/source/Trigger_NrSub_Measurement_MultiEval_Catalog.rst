Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:CATalog:SOURce

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:MEValuation:CATalog:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.NrSub.Measurement.MultiEval.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: