SfDefault
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:ADJustment:SFDefault

.. code-block:: python

	CONFigure:BASE:ADJustment:SFDefault



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.Adjustment.SfDefault.SfDefaultCls
	:members:
	:undoc-members:
	:noindex: