Arb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>[:ARB]:SOURce

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>[:ARB]:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.Gprf.Generator.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.generator.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Generator_Arb_Catalog.rst