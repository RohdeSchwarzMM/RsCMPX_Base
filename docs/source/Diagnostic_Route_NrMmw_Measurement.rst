Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ROUTe:NRMMw:MEASurement<Instance>:SPATh

.. code-block:: python

	DIAGnostic:ROUTe:NRMMw:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Route.NrMmw.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: