Event
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:OPERation:BIT<bitno>[:EVENt]

.. code-block:: python

	STATus:OPERation:BIT<bitno>[:EVENt]



.. autoclass:: RsCMPX_Base.Implementations.Status.Operation.Bit.Event.EventCls
	:members:
	:undoc-members:
	:noindex: