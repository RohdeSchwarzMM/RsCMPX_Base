Consistency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:INSTrument:CONSistency

.. code-block:: python

	DIAGnostic:INSTrument:CONSistency



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Instrument.Consistency.ConsistencyCls
	:members:
	:undoc-members:
	:noindex: