On
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:GENerator:CONDition:ON

.. code-block:: python

	STATus:GENerator:CONDition:ON



.. autoclass:: RsCMPX_Base.Implementations.Status.Generator.Condition.On.OnCls
	:members:
	:undoc-members:
	:noindex: