Salignment
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BASE:SALignment:MODE
	single: CONFigure:BASE:SALignment:SLOT

.. code-block:: python

	CONFigure:BASE:SALignment:MODE
	CONFigure:BASE:SALignment:SLOT



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.Salignment.SalignmentCls
	:members:
	:undoc-members:
	:noindex: