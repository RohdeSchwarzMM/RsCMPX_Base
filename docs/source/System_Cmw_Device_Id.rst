Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:CMW<n>:DEVice:ID

.. code-block:: python

	SYSTem:CMW<n>:DEVice:ID



.. autoclass:: RsCMPX_Base.Implementations.System.Cmw.Device.Id.IdCls
	:members:
	:undoc-members:
	:noindex: