Globale
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MODify:SYSTem:ATTenuation:CTABle:GLOBal

.. code-block:: python

	MODify:SYSTem:ATTenuation:CTABle:GLOBal



.. autoclass:: RsCMPX_Base.Implementations.Modify.System.Attenuation.CorrectionTable.Globale.GlobaleCls
	:members:
	:undoc-members:
	:noindex: