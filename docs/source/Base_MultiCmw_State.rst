State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:MCMW:STATe

.. code-block:: python

	FETCh:BASE:MCMW:STATe



.. autoclass:: RsCMPX_Base.Implementations.Base.MultiCmw.State.StateCls
	:members:
	:undoc-members:
	:noindex: