Reference
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.System.Base.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.reference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_Reference_Dc.rst
	System_Base_Reference_Frequency.rst
	System_Base_Reference_Phase.rst