Possible
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:ROUTing:POSSible

.. code-block:: python

	SYSTem:ROUTing:POSSible



.. autoclass:: RsCMPX_Base.Implementations.System.Routing.Possible.PossibleCls
	:members:
	:undoc-members:
	:noindex: