StopMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:STOPmode

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:STOPmode



.. autoclass:: RsCMPX_Base.Implementations.Trace.Remote.Mode.File.StopMode.StopModeCls
	:members:
	:undoc-members:
	:noindex: