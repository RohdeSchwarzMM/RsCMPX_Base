Ptransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:OPERation:BIT<bitno>:PTRansition

.. code-block:: python

	STATus:OPERation:BIT<bitno>:PTRansition



.. autoclass:: RsCMPX_Base.Implementations.Status.Operation.Bit.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex: