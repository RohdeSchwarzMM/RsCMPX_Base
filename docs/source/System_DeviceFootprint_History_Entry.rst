Entry
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DFPRint:HISTory:ENTRy

.. code-block:: python

	SYSTem:DFPRint:HISTory:ENTRy



.. autoclass:: RsCMPX_Base.Implementations.System.DeviceFootprint.History.Entry.EntryCls
	:members:
	:undoc-members:
	:noindex: