Ntransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<bitno>:NTRansition

.. code-block:: python

	STATus:QUEStionable:BIT<bitno>:NTRansition



.. autoclass:: RsCMPX_Base.Implementations.Status.Questionable.Bit.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: