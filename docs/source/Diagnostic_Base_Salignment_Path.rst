Path
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Base.Salignment.Path.PathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.base.salignment.path.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Base_Salignment_Path_Iq.rst
	Diagnostic_Base_Salignment_Path_Level.rst