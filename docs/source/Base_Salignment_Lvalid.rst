Lvalid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:LVALid

.. code-block:: python

	FETCh:BASE:SALignment:LVALid



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Lvalid.LvalidCls
	:members:
	:undoc-members:
	:noindex: