Gtr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:VXI<inst>:GTR

.. code-block:: python

	SYSTem:COMMunicate:VXI<inst>:GTR



.. autoclass:: RsCMPX_Base.Implementations.System.Communicate.Vxi.Gtr.GtrCls
	:members:
	:undoc-members:
	:noindex: