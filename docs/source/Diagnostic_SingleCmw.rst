SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:CMWS:LEDTest

.. code-block:: python

	DIAGnostic:CMWS:LEDTest



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: