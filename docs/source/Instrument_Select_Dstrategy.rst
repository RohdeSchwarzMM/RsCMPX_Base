Dstrategy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INSTrument[:SELect]:DSTRategy

.. code-block:: python

	INSTrument[:SELect]:DSTRategy



.. autoclass:: RsCMPX_Base.Implementations.Instrument.Select.Dstrategy.DstrategyCls
	:members:
	:undoc-members:
	:noindex: