Measurement
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Configure.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_IqRecorder.rst
	Configure_Gprf_Measurement_IqVsSlot.rst
	Configure_Gprf_Measurement_Power.rst