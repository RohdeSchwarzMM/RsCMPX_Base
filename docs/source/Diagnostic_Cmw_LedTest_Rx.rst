Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:CMW<variant>:LEDTest:RX

.. code-block:: python

	DIAGnostic:CMW<variant>:LEDTest:RX



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Cmw.LedTest.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: