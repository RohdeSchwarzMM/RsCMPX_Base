Identify
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STARt:BASE:MCMW:IDENtify

.. code-block:: python

	STARt:BASE:MCMW:IDENtify



.. autoclass:: RsCMPX_Base.Implementations.Base.MultiCmw.Identify.IdentifyCls
	:members:
	:undoc-members:
	:noindex: