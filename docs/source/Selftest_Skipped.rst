Skipped
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:SELFtest:SKIPped
	single: READ:SELFtest:SKIPped

.. code-block:: python

	FETCh:SELFtest:SKIPped
	READ:SELFtest:SKIPped



.. autoclass:: RsCMPX_Base.Implementations.Selftest.Skipped.SkippedCls
	:members:
	:undoc-members:
	:noindex: