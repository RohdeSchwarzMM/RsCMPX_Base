Set
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:TIME:HRTimer:ABSolute:SET

.. code-block:: python

	SYSTem:TIME:HRTimer:ABSolute:SET



.. autoclass:: RsCMPX_Base.Implementations.System.Time.HrTimer.Absolute.Set.SetCls
	:members:
	:undoc-members:
	:noindex: