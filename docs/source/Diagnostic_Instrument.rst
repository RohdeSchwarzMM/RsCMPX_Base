Instrument
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:INSTrument:LOAD
	single: DIAGnostic:INSTrument:UNLoad

.. code-block:: python

	DIAGnostic:INSTrument:LOAD
	DIAGnostic:INSTrument:UNLoad



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Instrument.InstrumentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.instrument.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Instrument_Application.rst
	Diagnostic_Instrument_Consistency.rst