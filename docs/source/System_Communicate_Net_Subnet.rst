Subnet
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:NET:SUBNet:MASK

.. code-block:: python

	SYSTem:COMMunicate:NET:SUBNet:MASK



.. autoclass:: RsCMPX_Base.Implementations.System.Communicate.Net.Subnet.SubnetCls
	:members:
	:undoc-members:
	:noindex: