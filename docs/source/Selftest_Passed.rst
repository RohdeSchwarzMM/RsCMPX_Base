Passed
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:SELFtest:PASSed
	single: READ:SELFtest:PASSed

.. code-block:: python

	FETCh:SELFtest:PASSed
	READ:SELFtest:PASSed



.. autoclass:: RsCMPX_Base.Implementations.Selftest.Passed.PassedCls
	:members:
	:undoc-members:
	:noindex: