Salignment
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Base.Salignment.SalignmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.base.salignment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Base_Salignment_Path.rst