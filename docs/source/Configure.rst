Configure
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base.rst
	Configure_Cmwd.rst
	Configure_FreqCorrection.rst
	Configure_Gprf.rst
	Configure_Mutex.rst
	Configure_Selftest.rst
	Configure_Semaphore.rst
	Configure_SingleCmw.rst
	Configure_Spoint.rst
	Configure_System.rst
	Configure_Tenvironment.rst