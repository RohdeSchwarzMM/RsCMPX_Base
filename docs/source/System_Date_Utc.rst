Utc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DATE:UTC

.. code-block:: python

	SYSTem:DATE:UTC



.. autoclass:: RsCMPX_Base.Implementations.System.Date.Utc.UtcCls
	:members:
	:undoc-members:
	:noindex: