TxImage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:TRACe:TXIMage

.. code-block:: python

	FETCh:BASE:SALignment:TRACe:TXIMage



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Trace.TxImage.TxImageCls
	:members:
	:undoc-members:
	:noindex: