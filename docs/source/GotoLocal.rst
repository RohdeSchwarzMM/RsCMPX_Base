GotoLocal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *GTL

.. code-block:: python

	*GTL



.. autoclass:: RsCMPX_Base.Implementations.GotoLocal.GotoLocalCls
	:members:
	:undoc-members:
	:noindex: