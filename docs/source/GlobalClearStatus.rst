GlobalClearStatus
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *GCLS

.. code-block:: python

	*GCLS



.. autoclass:: RsCMPX_Base.Implementations.GlobalClearStatus.GlobalClearStatusCls
	:members:
	:undoc-members:
	:noindex: