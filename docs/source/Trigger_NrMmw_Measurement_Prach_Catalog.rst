Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRMMw:MEASurement<Instance>:PRACh:CATalog:SOURce

.. code-block:: python

	TRIGger:NRMMw:MEASurement<Instance>:PRACh:CATalog:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.NrMmw.Measurement.Prach.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: