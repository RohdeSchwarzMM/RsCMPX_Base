Specific
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:BASE:LATest:SPECific

.. code-block:: python

	CALibration:BASE:LATest:SPECific



.. autoclass:: RsCMPX_Base.Implementations.Calibration.Base.Latest.Specific.SpecificCls
	:members:
	:undoc-members:
	:noindex: