Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:GPRF:MEASurement<Instance>:IQVSlot:TRIGger:SOURce

.. code-block:: python

	[CONFigure]:GPRF:MEASurement<Instance>:IQVSlot:TRIGger:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Configure.Gprf.Measurement.IqVsSlot.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: