Info
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SELFtest:INFO:PROGress

.. code-block:: python

	CONFigure:SELFtest:INFO:PROGress



.. autoclass:: RsCMPX_Base.Implementations.Configure.Selftest.Info.InfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.selftest.info.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Selftest_Info_Description.rst
	Configure_Selftest_Info_Message.rst