Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:GSM:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:GSM:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:GSM:MEASurement<Instance>:SPATh:COUNt
	ROUTe:GSM:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Base.Implementations.Route.Gsm.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: