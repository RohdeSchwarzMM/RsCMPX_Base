Factory
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BASE:PRODuct:OPTion:FACTory:CLEar

.. code-block:: python

	DIAGnostic:BASE:PRODuct:OPTion:FACTory:CLEar



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Base.Product.Option.Factory.FactoryCls
	:members:
	:undoc-members:
	:noindex: