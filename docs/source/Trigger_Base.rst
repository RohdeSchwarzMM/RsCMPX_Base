Base
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Trigger.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Base_Eout.rst
	Trigger_Base_ExtA.rst
	Trigger_Base_ExtB.rst
	Trigger_Base_Uinitiated.rst