History
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DFPRint:HISTory:COUNt

.. code-block:: python

	SYSTem:DFPRint:HISTory:COUNt



.. autoclass:: RsCMPX_Base.Implementations.System.DeviceFootprint.History.HistoryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.deviceFootprint.history.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_DeviceFootprint_History_Entry.rst