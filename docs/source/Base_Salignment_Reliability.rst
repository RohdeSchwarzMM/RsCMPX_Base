Reliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:RELiabiliy

.. code-block:: python

	FETCh:BASE:SALignment:RELiabiliy



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Reliability.ReliabilityCls
	:members:
	:undoc-members:
	:noindex: