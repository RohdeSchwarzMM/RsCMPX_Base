Globale
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: REMove:SYSTem:ATTenuation:CTABle:GLOBal

.. code-block:: python

	REMove:SYSTem:ATTenuation:CTABle:GLOBal



.. autoclass:: RsCMPX_Base.Implementations.Remove.System.Attenuation.CorrectionTable.Globale.GlobaleCls
	:members:
	:undoc-members:
	:noindex: