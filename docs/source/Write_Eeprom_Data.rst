Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: WRITe:EEPRom:DATA

.. code-block:: python

	WRITe:EEPRom:DATA



.. autoclass:: RsCMPX_Base.Implementations.Write.Eeprom.Data.DataCls
	:members:
	:undoc-members:
	:noindex: