Selftest
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CATalog:SELFtest
	single: CATalog:SELFtest:UPRofile

.. code-block:: python

	CATalog:SELFtest
	CATalog:SELFtest:UPRofile



.. autoclass:: RsCMPX_Base.Implementations.Catalog.Selftest.SelftestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.selftest.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Selftest_Selected.rst