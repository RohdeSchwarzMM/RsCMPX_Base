MultiCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:MCMW:REARrange

.. code-block:: python

	CONFigure:BASE:MCMW:REARrange



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.MultiCmw.MultiCmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.multiCmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_MultiCmw_Identify.rst