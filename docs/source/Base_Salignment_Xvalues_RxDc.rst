RxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:XVALues:RXDC

.. code-block:: python

	FETCh:BASE:SALignment:XVALues:RXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Xvalues.RxDc.RxDcCls
	:members:
	:undoc-members:
	:noindex: