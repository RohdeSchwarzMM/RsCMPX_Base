IfEqualizer
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.Correction.IfEqualizer.IfEqualizerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.correction.ifEqualizer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_Correction_IfEqualizer_Slot.rst