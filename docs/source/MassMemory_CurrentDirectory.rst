CurrentDirectory
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:CDIRectory

.. code-block:: python

	MMEMory:CDIRectory



.. autoclass:: RsCMPX_Base.Implementations.MassMemory.CurrentDirectory.CurrentDirectoryCls
	:members:
	:undoc-members:
	:noindex: