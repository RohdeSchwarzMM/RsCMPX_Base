MacroCreate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *DMC

.. code-block:: python

	*DMC



.. autoclass:: RsCMPX_Base.Implementations.MacroCreate.MacroCreateCls
	:members:
	:undoc-members:
	:noindex: