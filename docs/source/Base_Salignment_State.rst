State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:STATe

.. code-block:: python

	FETCh:BASE:SALignment:STATe



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.State.StateCls
	:members:
	:undoc-members:
	:noindex: