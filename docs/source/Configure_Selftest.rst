Selftest
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:SELFtest:ASMeas
	single: CONFigure:SELFtest:SCONdition
	single: CONFigure:SELFtest:REPetition
	single: CONFigure:SELFtest:SMODe
	single: CONFigure:SELFtest:EXECution

.. code-block:: python

	CONFigure:SELFtest:ASMeas
	CONFigure:SELFtest:SCONdition
	CONFigure:SELFtest:REPetition
	CONFigure:SELFtest:SMODe
	CONFigure:SELFtest:EXECution



.. autoclass:: RsCMPX_Base.Implementations.Configure.Selftest.SelftestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.selftest.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Selftest_Info.rst
	Configure_Selftest_Select.rst
	Configure_Selftest_Uprofile.rst