Off
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DISPlay:MONitor:OFF

.. code-block:: python

	SYSTem:DISPlay:MONitor:OFF



.. autoclass:: RsCMPX_Base.Implementations.System.Display.Monitor.Off.OffCls
	:members:
	:undoc-members:
	:noindex: