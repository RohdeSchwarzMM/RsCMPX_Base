Base
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FCONtrol

.. code-block:: python

	CONFigure:BASE:FCONtrol



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_Adjustment.rst
	Configure_Base_Correction.rst
	Configure_Base_FreqCorrection.rst
	Configure_Base_Ipcr.rst
	Configure_Base_IpSet.rst
	Configure_Base_Mmonitor.rst
	Configure_Base_MultiCmw.rst
	Configure_Base_Salignment.rst