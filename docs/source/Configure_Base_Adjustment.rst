Adjustment
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BASE:ADJustment:TYPE
	single: CONFigure:BASE:ADJustment:VALue
	single: CONFigure:BASE:ADJustment:SAVE

.. code-block:: python

	CONFigure:BASE:ADJustment:TYPE
	CONFigure:BASE:ADJustment:VALue
	CONFigure:BASE:ADJustment:SAVE



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.Adjustment.AdjustmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.adjustment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_Adjustment_SfDefault.rst