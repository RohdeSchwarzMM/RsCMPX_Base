Uncorrected
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Base.Correction.IfEqualizer.Trace.Gdelay.Uncorrected.UncorrectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.correction.ifEqualizer.trace.gdelay.uncorrected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Base_Correction_IfEqualizer_Trace_Gdelay_Uncorrected_Slot.rst