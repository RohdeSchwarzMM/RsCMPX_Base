SaveState
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *SAV

.. code-block:: python

	*SAV



.. autoclass:: RsCMPX_Base.Implementations.SaveState.SaveStateCls
	:members:
	:undoc-members:
	:noindex: