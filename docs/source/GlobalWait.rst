GlobalWait
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *GWAI

.. code-block:: python

	*GWAI



.. autoclass:: RsCMPX_Base.Implementations.GlobalWait.GlobalWaitCls
	:members:
	:undoc-members:
	:noindex: