Ptransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<bitno>:PTRansition

.. code-block:: python

	STATus:QUEStionable:BIT<bitno>:PTRansition



.. autoclass:: RsCMPX_Base.Implementations.Status.Questionable.Bit.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex: