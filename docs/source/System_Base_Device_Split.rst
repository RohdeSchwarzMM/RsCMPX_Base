Split
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:DEVice:SPLit

.. code-block:: python

	SYSTem:BASE:DEVice:SPLit



.. autoclass:: RsCMPX_Base.Implementations.System.Base.Device.Split.SplitCls
	:members:
	:undoc-members:
	:noindex: