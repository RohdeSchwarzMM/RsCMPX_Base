Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:COMPass:DBASe:TALogging:MODE

.. code-block:: python

	DIAGnostic:COMPass:DBASe:TALogging:MODE



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Compass.Dbase.TaLogging.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: