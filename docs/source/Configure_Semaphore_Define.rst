Define
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SEMaphore:DEFine

.. code-block:: python

	CONFigure:SEMaphore:DEFine



.. autoclass:: RsCMPX_Base.Implementations.Configure.Semaphore.Define.DefineCls
	:members:
	:undoc-members:
	:noindex: