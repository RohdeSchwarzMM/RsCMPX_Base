References
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FOOTprint:ELEMent:REFerences

.. code-block:: python

	DIAGnostic:FOOTprint:ELEMent:REFerences



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.FootPrint.Element.References.ReferencesCls
	:members:
	:undoc-members:
	:noindex: