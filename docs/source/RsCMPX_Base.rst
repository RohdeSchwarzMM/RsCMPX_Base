RsCMPX_Base API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCMPX_Base('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst32
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCMPX_Base.RsCMPX_Base
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Add.rst
	Base.rst
	Calibration.rst
	Catalog.rst
	Cmwd.rst
	Configure.rst
	Create.rst
	Diagnostic.rst
	Display.rst
	FirmwareUpdate.rst
	FormatPy.rst
	Get.rst
	GlobalClearStatus.rst
	GlobalWait.rst
	GotoLocal.rst
	HardCopy.rst
	Init.rst
	Instrument.rst
	MacroCreate.rst
	MassMemory.rst
	Modify.rst
	Procedure.rst
	RecallState.rst
	Remove.rst
	Route.rst
	SaveState.rst
	Selftest.rst
	Sense.rst
	Source.rst
	Status.rst
	System.rst
	Tenvironment.rst
	Trace.rst
	Trigger.rst
	TriggerInvoke.rst
	Unit.rst
	Write.rst