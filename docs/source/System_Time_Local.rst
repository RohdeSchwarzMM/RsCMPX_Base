Local
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:TIME:LOCal

.. code-block:: python

	SYSTem:TIME:LOCal



.. autoclass:: RsCMPX_Base.Implementations.System.Time.Local.LocalCls
	:members:
	:undoc-members:
	:noindex: