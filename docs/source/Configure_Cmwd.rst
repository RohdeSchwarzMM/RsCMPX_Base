Cmwd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWD:TIMeout

.. code-block:: python

	CONFigure:CMWD:TIMeout



.. autoclass:: RsCMPX_Base.Implementations.Configure.Cmwd.CmwdCls
	:members:
	:undoc-members:
	:noindex: