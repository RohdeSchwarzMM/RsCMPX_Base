Snumber
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:MCMW:SNUMber

.. code-block:: python

	FETCh:BASE:MCMW:SNUMber



.. autoclass:: RsCMPX_Base.Implementations.Base.MultiCmw.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: