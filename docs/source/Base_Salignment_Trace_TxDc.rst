TxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:TRACe:TXDC

.. code-block:: python

	FETCh:BASE:SALignment:TRACe:TXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Trace.TxDc.TxDcCls
	:members:
	:undoc-members:
	:noindex: