BgInfo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BGINfo:CATalog

.. code-block:: python

	DIAGnostic:BGINfo:CATalog



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.BgInfo.BgInfoCls
	:members:
	:undoc-members:
	:noindex: