Correction
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.Correction.CorrectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.correction.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_Correction_IfEqualizer.rst