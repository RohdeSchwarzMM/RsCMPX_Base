State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:CMWD:STATe

.. code-block:: python

	FETCh:CMWD:STATe



.. autoclass:: RsCMPX_Base.Implementations.Cmwd.State.StateCls
	:members:
	:undoc-members:
	:noindex: