FreqCorrection
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:FDCorrection:DEACtivate
	single: CONFigure:FDCorrection:DEACtivate:ALL

.. code-block:: python

	CONFigure:FDCorrection:DEACtivate
	CONFigure:FDCorrection:DEACtivate:ALL



.. autoclass:: RsCMPX_Base.Implementations.Configure.FreqCorrection.FreqCorrectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.freqCorrection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_FreqCorrection_Activate.rst
	Configure_FreqCorrection_Usage.rst