Usage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWS:FDCorrection:USAGe

.. code-block:: python

	CONFigure:CMWS:FDCorrection:USAGe



.. autoclass:: RsCMPX_Base.Implementations.Configure.SingleCmw.FreqCorrection.Usage.UsageCls
	:members:
	:undoc-members:
	:noindex: