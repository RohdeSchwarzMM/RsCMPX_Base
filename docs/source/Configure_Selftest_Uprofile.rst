Uprofile
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:SELFtest:UPRofile:SAVE
	single: CONFigure:SELFtest:UPRofile:LOAD

.. code-block:: python

	CONFigure:SELFtest:UPRofile:SAVE
	CONFigure:SELFtest:UPRofile:LOAD



.. autoclass:: RsCMPX_Base.Implementations.Configure.Selftest.Uprofile.UprofileCls
	:members:
	:undoc-members:
	:noindex: