Description
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:OPTion:DESCription

.. code-block:: python

	SYSTem:BASE:OPTion:DESCription



.. autoclass:: RsCMPX_Base.Implementations.System.Base.Option.Description.DescriptionCls
	:members:
	:undoc-members:
	:noindex: