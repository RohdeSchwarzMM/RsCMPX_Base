Sense
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Sense.SenseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Base.rst
	Sense_FirmwareUpdate.rst
	Sense_Selftest.rst