Selftest
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ABORt:SELFtest
	single: STOP:SELFtest
	single: FETCh:SELFtest
	single: READ:SELFtest

.. code-block:: python

	ABORt:SELFtest
	STOP:SELFtest
	FETCh:SELFtest
	READ:SELFtest



.. autoclass:: RsCMPX_Base.Implementations.Selftest.SelftestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.selftest.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Selftest_Failed.rst
	Selftest_Passed.rst
	Selftest_Skipped.rst
	Selftest_State.rst