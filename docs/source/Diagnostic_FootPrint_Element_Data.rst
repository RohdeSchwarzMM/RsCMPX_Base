Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FOOTprint:ELEMent:DATA

.. code-block:: python

	DIAGnostic:FOOTprint:ELEMent:DATA



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.FootPrint.Element.Data.DataCls
	:members:
	:undoc-members:
	:noindex: