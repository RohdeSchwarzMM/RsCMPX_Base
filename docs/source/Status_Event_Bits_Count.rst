Count
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:EVENt:BITS:COUNt

.. code-block:: python

	STATus:EVENt:BITS:COUNt



.. autoclass:: RsCMPX_Base.Implementations.Status.Event.Bits.Count.CountCls
	:members:
	:undoc-members:
	:noindex: