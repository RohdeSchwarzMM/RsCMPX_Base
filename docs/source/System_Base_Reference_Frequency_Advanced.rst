Advanced
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.System.Base.Reference.Frequency.Advanced.AdvancedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.reference.frequency.advanced.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_Reference_Frequency_Advanced_Source.rst