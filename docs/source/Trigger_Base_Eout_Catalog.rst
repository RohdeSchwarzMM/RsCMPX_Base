Catalog
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Trigger.Base.Eout.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.base.eout.catalog.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Base_Eout_Catalog_Source.rst