CorrectionTable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:DELete
	single: CONFigure:BASE:FDCorrection:CTABle:DELete:ALL

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:DELete
	CONFigure:BASE:FDCorrection:CTABle:DELete:ALL



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.CorrectionTable.CorrectionTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.freqCorrection.correctionTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_FreqCorrection_CorrectionTable_Add.rst
	Configure_Base_FreqCorrection_CorrectionTable_Catalog.rst
	Configure_Base_FreqCorrection_CorrectionTable_Count.rst
	Configure_Base_FreqCorrection_CorrectionTable_Create.rst
	Configure_Base_FreqCorrection_CorrectionTable_Details.rst
	Configure_Base_FreqCorrection_CorrectionTable_Erase.rst
	Configure_Base_FreqCorrection_CorrectionTable_Exist.rst
	Configure_Base_FreqCorrection_CorrectionTable_Length.rst