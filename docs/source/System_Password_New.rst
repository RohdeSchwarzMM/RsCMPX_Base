New
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PASSword:NEW

.. code-block:: python

	SYSTem:PASSword:NEW



.. autoclass:: RsCMPX_Base.Implementations.System.Password.New.NewCls
	:members:
	:undoc-members:
	:noindex: