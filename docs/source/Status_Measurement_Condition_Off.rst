Off
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:MEASurement:CONDition:OFF

.. code-block:: python

	STATus:MEASurement:CONDition:OFF



.. autoclass:: RsCMPX_Base.Implementations.Status.Measurement.Condition.Off.OffCls
	:members:
	:undoc-members:
	:noindex: