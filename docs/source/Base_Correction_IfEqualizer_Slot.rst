Slot<Slot>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.base.correction.ifEqualizer.slot.repcap_slot_get()
	driver.base.correction.ifEqualizer.slot.repcap_slot_set(repcap.Slot.Nr1)





.. autoclass:: RsCMPX_Base.Implementations.Base.Correction.IfEqualizer.Slot.SlotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.correction.ifEqualizer.slot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Base_Correction_IfEqualizer_Slot_RxFilter.rst
	Base_Correction_IfEqualizer_Slot_TxFilter.rst