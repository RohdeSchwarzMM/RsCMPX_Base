Off
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:GENerator:CONDition:OFF

.. code-block:: python

	STATus:GENerator:CONDition:OFF



.. autoclass:: RsCMPX_Base.Implementations.Status.Generator.Condition.Off.OffCls
	:members:
	:undoc-members:
	:noindex: