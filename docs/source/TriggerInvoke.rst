TriggerInvoke
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *TRG

.. code-block:: python

	*TRG



.. autoclass:: RsCMPX_Base.Implementations.TriggerInvoke.TriggerInvokeCls
	:members:
	:undoc-members:
	:noindex: