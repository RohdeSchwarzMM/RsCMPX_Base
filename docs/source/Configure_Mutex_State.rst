State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:MUTex:STATe

.. code-block:: python

	CONFigure:MUTex:STATe



.. autoclass:: RsCMPX_Base.Implementations.Configure.Mutex.State.StateCls
	:members:
	:undoc-members:
	:noindex: