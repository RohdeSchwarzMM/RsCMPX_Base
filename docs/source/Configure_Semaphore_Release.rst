Release
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SEMaphore:RELease

.. code-block:: python

	CONFigure:SEMaphore:RELease



.. autoclass:: RsCMPX_Base.Implementations.Configure.Semaphore.Release.ReleaseCls
	:members:
	:undoc-members:
	:noindex: