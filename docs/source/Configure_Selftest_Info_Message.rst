Message
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SELFtest:INFO:MESSage

.. code-block:: python

	CONFigure:SELFtest:INFO:MESSage



.. autoclass:: RsCMPX_Base.Implementations.Configure.Selftest.Info.Message.MessageCls
	:members:
	:undoc-members:
	:noindex: