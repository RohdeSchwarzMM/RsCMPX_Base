Base
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration:BASE:ALL
	single: CALibration:BASE:ACFile

.. code-block:: python

	CALibration:BASE:ALL
	CALibration:BASE:ACFile



.. autoclass:: RsCMPX_Base.Implementations.Calibration.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Base_Ipc.rst
	Calibration_Base_Ipcr.rst
	Calibration_Base_Latest.rst