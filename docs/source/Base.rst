Base
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Base_Buffer.rst
	Base_Correction.rst
	Base_Ipc.rst
	Base_MultiCmw.rst
	Base_Salignment.rst