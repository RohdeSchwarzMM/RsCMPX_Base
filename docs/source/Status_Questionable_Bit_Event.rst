Event
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<bitno>[:EVENt]

.. code-block:: python

	STATus:QUEStionable:BIT<bitno>[:EVENt]



.. autoclass:: RsCMPX_Base.Implementations.Status.Questionable.Bit.Event.EventCls
	:members:
	:undoc-members:
	:noindex: