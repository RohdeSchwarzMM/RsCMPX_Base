Protocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:COMPass:DBASe:RLOGging:PROTocol

.. code-block:: python

	DIAGnostic:COMPass:DBASe:RLOGging:PROTocol



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Compass.Dbase.Rlogging.Protocol.ProtocolCls
	:members:
	:undoc-members:
	:noindex: