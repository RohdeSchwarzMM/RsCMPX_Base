All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWS:FDCorrection:DEACtivate:TX:ALL

.. code-block:: python

	CONFigure:CMWS:FDCorrection:DEACtivate:TX:ALL



.. autoclass:: RsCMPX_Base.Implementations.Configure.SingleCmw.FreqCorrection.Deactivate.Tx.All.AllCls
	:members:
	:undoc-members:
	:noindex: