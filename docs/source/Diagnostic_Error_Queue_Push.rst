Push
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ERRor:QUEue:PUSH

.. code-block:: python

	DIAGnostic:ERRor:QUEue:PUSH



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Error.Queue.Push.PushCls
	:members:
	:undoc-members:
	:noindex: