Edevice
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:EDEVice

.. code-block:: python

	[CONFigure]:SYSTem:EDEVice



.. autoclass:: RsCMPX_Base.Implementations.Configure.System.Edevice.EdeviceCls
	:members:
	:undoc-members:
	:noindex: