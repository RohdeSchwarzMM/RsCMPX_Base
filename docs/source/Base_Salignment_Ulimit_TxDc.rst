TxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:ULIMit:TXDC

.. code-block:: python

	FETCh:BASE:SALignment:ULIMit:TXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Ulimit.TxDc.TxDcCls
	:members:
	:undoc-members:
	:noindex: