State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:LOAD:STATe

.. code-block:: python

	MMEMory:LOAD:STATe



.. autoclass:: RsCMPX_Base.Implementations.MassMemory.Load.State.StateCls
	:members:
	:undoc-members:
	:noindex: