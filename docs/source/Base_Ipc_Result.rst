Result
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:IPC:RESult

.. code-block:: python

	FETCh:BASE:IPC:RESult



.. autoclass:: RsCMPX_Base.Implementations.Base.Ipc.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: