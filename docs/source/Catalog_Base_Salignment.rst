Salignment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:BASE:SALignment:SLOT

.. code-block:: python

	CATalog:BASE:SALignment:SLOT



.. autoclass:: RsCMPX_Base.Implementations.Catalog.Base.Salignment.SalignmentCls
	:members:
	:undoc-members:
	:noindex: