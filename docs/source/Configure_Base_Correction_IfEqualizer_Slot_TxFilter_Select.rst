Select
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:CORRection:IFEQualizer:SLOT<Slot>:TXFilter:SELect

.. code-block:: python

	CONFigure:BASE:CORRection:IFEQualizer:SLOT<Slot>:TXFilter:SELect



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.Correction.IfEqualizer.Slot.TxFilter.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: