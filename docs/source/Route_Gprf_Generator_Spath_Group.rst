Group
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:GENerator<Instance>:SPATh:GROup

.. code-block:: python

	ROUTe:GPRF:GENerator<Instance>:SPATh:GROup



.. autoclass:: RsCMPX_Base.Implementations.Route.Gprf.Generator.Spath.Group.GroupCls
	:members:
	:undoc-members:
	:noindex: