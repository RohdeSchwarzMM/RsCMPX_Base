FreqCorrection
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:BASE:FDCorrection:SAV
	single: CONFigure:BASE:FDCorrection:RCL

.. code-block:: python

	CONFigure:BASE:FDCorrection:SAV
	CONFigure:BASE:FDCorrection:RCL



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.FreqCorrectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.base.freqCorrection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Base_FreqCorrection_CorrectionTable.rst