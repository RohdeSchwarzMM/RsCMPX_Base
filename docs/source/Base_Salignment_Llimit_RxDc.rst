RxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:LLIMit:RXDC

.. code-block:: python

	FETCh:BASE:SALignment:LLIMit:RXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Llimit.RxDc.RxDcCls
	:members:
	:undoc-members:
	:noindex: