Trace
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.salignment.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Base_Salignment_Trace_RxDc.rst
	Base_Salignment_Trace_RxImage.rst
	Base_Salignment_Trace_TxDc.rst
	Base_Salignment_Trace_TxImage.rst