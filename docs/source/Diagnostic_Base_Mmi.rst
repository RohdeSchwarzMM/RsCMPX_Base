Mmi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BASE:MMI:VERSion

.. code-block:: python

	DIAGnostic:BASE:MMI:VERSion



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Base.Mmi.MmiCls
	:members:
	:undoc-members:
	:noindex: