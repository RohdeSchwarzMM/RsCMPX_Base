Identify
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:MCMW:IDENtify:BTIMe

.. code-block:: python

	CONFigure:BASE:MCMW:IDENtify:BTIMe



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.MultiCmw.Identify.IdentifyCls
	:members:
	:undoc-members:
	:noindex: