Ssync
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:BASE:SSYNc:MODE
	single: SYSTem:BASE:SSYNc:OFFSet

.. code-block:: python

	SYSTem:BASE:SSYNc:MODE
	SYSTem:BASE:SSYNc:OFFSet



.. autoclass:: RsCMPX_Base.Implementations.System.Base.Ssync.SsyncCls
	:members:
	:undoc-members:
	:noindex: