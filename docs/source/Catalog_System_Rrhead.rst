Rrhead
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SYSTem:RRHead

.. code-block:: python

	CATalog:SYSTem:RRHead



.. autoclass:: RsCMPX_Base.Implementations.Catalog.System.Rrhead.RrheadCls
	:members:
	:undoc-members:
	:noindex: