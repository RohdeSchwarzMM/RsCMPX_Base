Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:SOCKet<inst>:MODE

.. code-block:: python

	SYSTem:COMMunicate:SOCKet<inst>:MODE



.. autoclass:: RsCMPX_Base.Implementations.System.Communicate.Socket.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: