Ntransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:OPERation:BIT<bitno>:NTRansition

.. code-block:: python

	STATus:OPERation:BIT<bitno>:NTRansition



.. autoclass:: RsCMPX_Base.Implementations.Status.Operation.Bit.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: