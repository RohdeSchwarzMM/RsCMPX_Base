Cataloge
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:CONDition:BITS:CATaloge

.. code-block:: python

	STATus:CONDition:BITS:CATaloge



.. autoclass:: RsCMPX_Base.Implementations.Status.Condition.Bits.Cataloge.CatalogeCls
	:members:
	:undoc-members:
	:noindex: