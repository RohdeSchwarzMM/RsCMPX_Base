Count
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:COUNt

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:COUNt



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.CorrectionTable.Count.CountCls
	:members:
	:undoc-members:
	:noindex: