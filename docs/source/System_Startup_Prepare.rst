Prepare
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:STARtup:PREPare:FDEFault

.. code-block:: python

	SYSTem:STARtup:PREPare:FDEFault



.. autoclass:: RsCMPX_Base.Implementations.System.Startup.Prepare.PrepareCls
	:members:
	:undoc-members:
	:noindex: