Erase
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:ERASe

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:ERASe



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.CorrectionTable.Erase.EraseCls
	:members:
	:undoc-members:
	:noindex: