Failed
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:SELFtest:FAILed
	single: READ:SELFtest:FAILed

.. code-block:: python

	FETCh:SELFtest:FAILed
	READ:SELFtest:FAILed



.. autoclass:: RsCMPX_Base.Implementations.Selftest.Failed.FailedCls
	:members:
	:undoc-members:
	:noindex: