SubMonitor
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.System.Base.IpSet.SubMonitor.SubMonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.base.ipSet.subMonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Base_IpSet_SubMonitor_Refresh.rst