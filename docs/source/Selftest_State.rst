State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:SELFtest:STATe

.. code-block:: python

	FETCh:SELFtest:STATe



.. autoclass:: RsCMPX_Base.Implementations.Selftest.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.selftest.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Selftest_State_All.rst