Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:ADD

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:ADD



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.FreqCorrection.CorrectionTable.Add.AddCls
	:members:
	:undoc-members:
	:noindex: