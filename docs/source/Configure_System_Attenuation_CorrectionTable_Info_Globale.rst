Globale
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:ATTenuation:CTABle:INFO:GLOBal

.. code-block:: python

	[CONFigure]:SYSTem:ATTenuation:CTABle:INFO:GLOBal



.. autoclass:: RsCMPX_Base.Implementations.Configure.System.Attenuation.CorrectionTable.Info.Globale.GlobaleCls
	:members:
	:undoc-members:
	:noindex: