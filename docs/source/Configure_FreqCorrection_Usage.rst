Usage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:FDCorrection:USAGe

.. code-block:: python

	CONFigure:FDCorrection:USAGe



.. autoclass:: RsCMPX_Base.Implementations.Configure.FreqCorrection.Usage.UsageCls
	:members:
	:undoc-members:
	:noindex: