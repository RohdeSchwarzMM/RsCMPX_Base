Adjustment
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Source.Base.Adjustment.AdjustmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.base.adjustment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Base_Adjustment_State.rst