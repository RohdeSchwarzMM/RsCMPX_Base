MultiCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:BASE:MCMW

.. code-block:: python

	INITiate:BASE:MCMW



.. autoclass:: RsCMPX_Base.Implementations.Base.MultiCmw.MultiCmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.base.multiCmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Base_MultiCmw_Identify.rst
	Base_MultiCmw_Snumber.rst
	Base_MultiCmw_State.rst