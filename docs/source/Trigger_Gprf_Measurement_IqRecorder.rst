IqRecorder
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.Gprf.Measurement.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.measurement.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Measurement_IqRecorder_Catalog.rst