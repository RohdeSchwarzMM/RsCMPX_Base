Pending
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:GENerator:CONDition:PENDing

.. code-block:: python

	STATus:GENerator:CONDition:PENDing



.. autoclass:: RsCMPX_Base.Implementations.Status.Generator.Condition.Pending.PendingCls
	:members:
	:undoc-members:
	:noindex: