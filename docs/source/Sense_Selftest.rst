Selftest
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Sense.Selftest.SelftestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.selftest.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Selftest_State.rst