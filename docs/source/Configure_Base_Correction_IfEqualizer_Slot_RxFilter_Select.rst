Select
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter:SELect

.. code-block:: python

	CONFigure:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter:SELect



.. autoclass:: RsCMPX_Base.Implementations.Configure.Base.Correction.IfEqualizer.Slot.RxFilter.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: