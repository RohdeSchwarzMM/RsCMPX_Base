SdReached
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:MEASurement:CONDition:SDReached

.. code-block:: python

	STATus:MEASurement:CONDition:SDReached



.. autoclass:: RsCMPX_Base.Implementations.Status.Measurement.Condition.SdReached.SdReachedCls
	:members:
	:undoc-members:
	:noindex: