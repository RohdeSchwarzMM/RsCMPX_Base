Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:MEValuation:CATalog:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:MEValuation:CATalog:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Trigger.Bluetooth.Measurement.MultiEval.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: