Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:GPRF:MEASurement<Instance>:IQRecorder:TRIGger:SOURce

.. code-block:: python

	[CONFigure]:GPRF:MEASurement<Instance>:IQRecorder:TRIGger:SOURce



.. autoclass:: RsCMPX_Base.Implementations.Configure.Gprf.Measurement.IqRecorder.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: