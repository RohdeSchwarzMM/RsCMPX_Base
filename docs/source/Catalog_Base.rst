Base
----------------------------------------





.. autoclass:: RsCMPX_Base.Implementations.Catalog.Base.BaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.base.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Base_Correction.rst
	Catalog_Base_Salignment.rst