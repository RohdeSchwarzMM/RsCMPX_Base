Get
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: GET:XVALues

.. code-block:: python

	GET:XVALues



.. autoclass:: RsCMPX_Base.Implementations.Get.GetCls
	:members:
	:undoc-members:
	:noindex: