TxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:XVALues:TXDC

.. code-block:: python

	FETCh:BASE:SALignment:XVALues:TXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Xvalues.TxDc.TxDcCls
	:members:
	:undoc-members:
	:noindex: