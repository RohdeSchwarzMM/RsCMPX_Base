Selected
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SELFtest:SELected

.. code-block:: python

	CATalog:SELFtest:SELected



.. autoclass:: RsCMPX_Base.Implementations.Catalog.Selftest.Selected.SelectedCls
	:members:
	:undoc-members:
	:noindex: