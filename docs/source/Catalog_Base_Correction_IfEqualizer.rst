IfEqualizer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:BASE:CORRection:IFEQualizer:SNAMe

.. code-block:: python

	CATalog:BASE:CORRection:IFEQualizer:SNAMe



.. autoclass:: RsCMPX_Base.Implementations.Catalog.Base.Correction.IfEqualizer.IfEqualizerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.base.correction.ifEqualizer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Base_Correction_IfEqualizer_Slot.rst