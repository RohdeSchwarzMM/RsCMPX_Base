Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:TENVironment:SPATh:CTABle:TX

.. code-block:: python

	ADD:TENVironment:SPATh:CTABle:TX



.. autoclass:: RsCMPX_Base.Implementations.Add.Tenvironment.Spath.CorrectionTable.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: