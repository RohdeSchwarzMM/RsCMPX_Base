RxDc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:SALignment:ULIMit:RXDC

.. code-block:: python

	FETCh:BASE:SALignment:ULIMit:RXDC



.. autoclass:: RsCMPX_Base.Implementations.Base.Salignment.Ulimit.RxDc.RxDcCls
	:members:
	:undoc-members:
	:noindex: