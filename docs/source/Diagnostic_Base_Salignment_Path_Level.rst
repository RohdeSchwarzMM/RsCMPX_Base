Level
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:BASE:SALignment:PATH:LEVel:STARt
	single: DIAGnostic:BASE:SALignment:PATH:LEVel:STATe
	single: DIAGnostic:BASE:SALignment:PATH:LEVel

.. code-block:: python

	DIAGnostic:BASE:SALignment:PATH:LEVel:STARt
	DIAGnostic:BASE:SALignment:PATH:LEVel:STATe
	DIAGnostic:BASE:SALignment:PATH:LEVel



.. autoclass:: RsCMPX_Base.Implementations.Diagnostic.Base.Salignment.Path.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: